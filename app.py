from flask_hal import HAL, document
from flask_hal.link import Link
from flask_hal.document import Document, Embedded
from flask_cors import CORS
from flask import Flask, request, render_template,send_from_directory, make_response, session, flash, url_for, g
from dotenv import load_dotenv
from os import getenv
from bcrypt import hashpw,gensalt, checkpw
from flask_session import Session
from datetime import datetime, timedelta
import secrets
from redis import StrictRedis
from jwt import encode, decode
import json
import uuid
import requests
import re
import math
import yagmail
import html
import time

load_dotenv()
REDIS_HOST = getenv("REDIS_HOST")
REDIS_PASS = getenv("REDIS_PASS")
JWT_SECRET = getenv("JWT_SECRET")
JWT_EXP = int(getenv("JWT_EXP"))

db = StrictRedis(REDIS_HOST, db=20, password=REDIS_PASS)

SESSION_TYPE = "redis"
SESSION_REDIS = db
app = Flask(__name__, static_url_path="/static")
app.config.from_object(__name__)
app.secret_key = secrets.token_urlsafe(32)
CORS(app)
sess = Session(app)

EMAIL = getenv("EMAIL_LOG")
EMAIL_PASS = getenv("EMAIL_PASS")

yag = yagmail.SMTP(EMAIL, EMAIL_PASS)


load_dotenv()
REDIS_HOST = getenv("REDIS_HOST")
REDIS_PASS = getenv("REDIS_PASS")
db = StrictRedis(REDIS_HOST, db=20, password=REDIS_PASS)

SESSION_TYPE = "redis"
SESSION_REDIS = db
app = Flask(__name__, static_url_path="/static")
app.config.from_object(__name__)
app.secret_key = secrets.token_urlsafe(32)
sess = Session(app)



@app.route('/')
def main():
    return render_template('main.html')

@app.route('/sign-up')
def singUp():
    return render_template('signUp.html')


@app.route('/register', methods=["GET"])
def register():
    return render_template('register.html')

@app.route('/register', methods=["POST"])
def register_post():
    firstname = request.form.get('firstname')
    lastname =  request.form.get('lastname')
    email = request.form.get('email')
    password =  request.form.get('password')
    login =  request.form.get('login')
    if  not firstname:
        flash("No first name provided")
    if  not re.match(r"^([A-Z]|Ą|Ć|Ę|Ł|Ń|Ś|Ż|Ź|Ó)([a-z]|ą|ę|ł|ń|ó|ś|ż|ź)+$", firstname):
        make_response("Something went wrong", 403)

    if  not lastname:
        flash("No lastname name provided")
    if  not re.match(r"^([A-Z]|Ą|Ć|Ę|Ł|Ń|Ś|Ż|Ź|Ó)([a-z]|ą|ę|ł|ń|ó|ś|ż|ź)+$", lastname):
        make_response("Something went wrong", 403)
    
    if  not email:
        flash("No email name provided")
    if not re.match(r"""^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$""", email):
        make_response("Something went wrong", 403)

    if  not password:
        flash("No password name provided")
    if  not re.match(r"^(?=.*[a-z])?(?=.*[A-Z])?(?=.*\d)?(?=.*[@$!%*?&])?[A-Za-z\d@$!%*?&]{8,25}$", password):
        make_response("Something went wrong", 403)

    if  not login:
        flash("No login name provided")
    if  not re.match(r"^([a-zA-Z][0-9]*){4,12}$", login):
        make_response("Something went wrong", 403)

    if  not isStrong(password):
        flash("Password is to weak. Use capital letters, numbers, special characters ")
        return redirect(url_for('register'))

    if email and login and password: 
        if is_user(login):
            flash("user already registred")
            return redirect(url_for('register'))
        
        success = save_user(firstname, lastname, email, password, login)
        if not success:
            flash("Error while sawing user")
            return redirect(url_for('register_form'))

    return redirect(url_for('login'))
    
@app.route('/login', methods=["GET"])
def login():
    return render_template('login.html')

@app.route('/login', methods=["POST"])
def login_post():

    login = request.form.get("login")
    password = request.form.get("password")

    if not login or not password:
        flash("Missing username and/or password")
        return redirect(url_for("login"))

    if  not re.match(r"^([a-zA-Z][0-9]*){4,12}$", login):
        make_response("Something went wrong", 403)

    if  not re.match(r"^(?=.*[a-z])?(?=.*[A-Z])?(?=.*\d)?(?=.*[@$!%*?&])?[A-Za-z\d@$!%*?&]{8,25}$", password):
        make_response("Something went wrong", 403)
    
    make_delay(login)

    if not verify_user(login, password):
        flash("Invalid username and/or password")
        return redirect(url_for("login"))


  
   
    flash(f"Welcome {login}!")
    session["login"] = login
    session["logged_at"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return redirect(url_for("main"))

def make_delay(login):
    log = db.hget(f"user:{login}", "log")
    log = log.decode()

    if re.match(r"^[T|F]*[F]{3}$", log):
        time.sleep(1)
    if re.match(r"^[T|F]*[F]{4}$", log):
        time.sleep(2)
    
    if re.match(r"^[T|F]*[F]{5,}$", log):
        time.sleep(3)
        contents = [
            f"Dear {login} somone try to log in to your account 5 or more times "
        ]
        email = db.hget(f"user:{login}", "email").decode()
        yag.send(email, 'Your new password', contents)

    if len(log) > 10:
        db.hset(f"user:{login}", "log", "T")


@app.route('/logout')
def logout():
    session.clear()
    flash("Logged out!")
    return redirect(url_for("main"))

  
@app.route('/notes')
def notes(): 
    return render_notes()

@app.route('/notes/public', methods=["POST"])
def notes_post_public():
    if g.user is None:
        return'Not authorized', 401

    note = request.form.get('note')

    if not note:
       make_response("Something went wrong.", 500)

    if len(note) > 50:
        make_response("Something went wrong", 403)

    create_public_note(note)
    return render_notes()

@app.route('/notes', methods=["POST"])
def notes_post():
    if g.user is None:
        return'Not authorized', 401

    note = request.form.get('note')
    user = request.form.get('user')

    if not note:
       make_response("something went wrong.", 500)
    if len(note) > 50:
        make_response("Something went wrong", 403)

    if not user:
       make_response("something went wrong.", 500)

    if not re.match(r"^([a-zA-Z][0-9]*){4,12}$", user):
        make_response("something went wrong.", 500)
    

    create_note(note,user)

    return render_notes()

@app.route('/notes/public', methods=["DELETE"])
def notes_delete_public():
    id = request.form.get('id')
    
    if not id:
        make_response("something went wrong.", 500)
    
    if not re.match(r"^[a-zA-Z0-9|-]*$", id):
        make_response("Something went wrong", 403)
    
    try:
        db.hdel(f"notes", f"note_{id}")
    except Exception:
        return make_response("Oops!", 400)
    
    return render_notes()


@app.route('/notes', methods=["DELETE"])
def notes_delete():
    if g.user is None:
        return'Not authorized', 401
    
    id = request.form.get('id')

    if not id:
       make_response("something went wrong.", 500)
    
    if not re.match(r"^[a-zA-Z0-9|-]*$", id):
        make_response("Something went wrong", 403)
    
    try:
        db.hdel(f"user:{g.user}:note", f"note_{id}")
    except Exception:
        return make_response("Oops!", 400)
    
    return render_notes()


def create_public_note(text):
    id = str(uuid.uuid4())
    note = {'id' :id, 'note': text }
    try:
        db.hset( f"notes", f"note_{id}", json.dumps(note))
    except Exception:
        return make_response("something went wrong.", 500) 

def create_note(text,user):
    id = str(uuid.uuid4())
    note = {'id' :id, 'note': text }
    try:
        db.hset(f"user:{user}:note", f"note_{id}", json.dumps(note))
    except Exception:
        return make_response("something went wrong.", 500) 

def get_notes(user):
    notes = {}
    user_notes = db.hgetall(f"user:{user}:note")

    for id in user_notes:
        note= user_notes[id].decode()
        note = json.loads(note)
        notes[id.decode()] = note
    
    return notes

def get_all_users():
    jusers = db.hgetall('users')
    users = []

    for login in jusers:
        user = jusers[login].decode()
        users.append(user)
    
    return users

def get_notes_public():
    Jnotes = {}
    notes = db.hgetall(f"notes")

    for id in notes:
        note = notes[id].decode()
        note = json.loads(note)
        Jnotes[id.decode()] = note
    return Jnotes


def render_notes():
    users = get_all_users()
    
    pNotes = []
    notes = get_notes_public()
    for note in notes:
        pNotes.append({'id' : notes[note]['id'] , 'note':  html.escape(notes[note]['note']) })

    user  = g.user
    notes = []
    if g.user is  not None:
        dnotes = get_notes(user)
        for note in dnotes:
            notes.append({'id' : dnotes[note]['id'] , 'note':  html.escape(dnotes[note]['note']) })

    return render_template('notes.html', pNotes=pNotes, notes = notes,users = users)



@app.route('/changePassword')
def changePasswd():
    if g.user is None:
        return'Not authorized', 401
    return render_template('changePasswd.html')

@app.route('/changePassword', methods=["POST"])
def changePasswd_Put():
    if g.user is None:
        return'Not authorized', 401
    user = g.user

    oldPassword = request.form.get('oldPassword')
    newPassword =  request.form.get('newPassword')

    if  not oldPassword:
        make_response("Something went wrong", 500)
    if  not newPassword:
        make_response("Something went wrong", 500)

    if  not re.match(r"^(?=.*[a-z])?(?=.*[A-Z])?(?=.*\d)?(?=.*[@$!%*?&])?[A-Za-z\d@$!%*?&]{8,25}$", oldPassword):
        make_response("Something went wrong", 403)
    if  not re.match(r"^(?=.*[a-z])?(?=.*[A-Z])?(?=.*\d)?(?=.*[@$!%*?&])?[A-Za-z\d@$!%*?&]{8,25}$", newPassword):
        make_response("Something went wrong", 403)
    
    if  not isStrong(newPassword):
        flash("Password is to weak. Use capital letters, numbers, special characters ")
        return redirect(url_for('changePasswd'))

    isTheSamePassword = verify_user(user, oldPassword)

    if isTheSamePassword:
        hashed_password = hashpw(newPassword.encode('utf-8'), gensalt(4))
        try:
            db.hset(f"user:{user}", "password", hashed_password)
        except Exception:
            return make_response("something went wrong.", 500)   

        flash("Password is changed")
        return render_template('main.html')
    else:
        flash("Wrong Password")
        return redirect(url_for('changePasswd'))


@app.route('/forgotten')
def getPasswd():
    return render_template('forgottenPassword.html')

@app.route('/forgotten', methods=["POST"])
def getPasswd_Post():
    login = request.form.get("login")
    email = request.form.get("email")

    if  not email:
        make_response("Something went wrong", 500)

    if not re.match(r"""^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$""", email):
        make_response("Something went wrong", 403)

    if  not login:
        flash("No login name provided")
    if  not re.match(r"^([a-zA-Z][0-9]*){4,12}$", login):
        make_response("Something went wrong", 500)

    if not is_user(login):
        flash("Wrong data")
        return render_template('forgottenPassword.html')

    if not is_the_same_email(login,email):
        flash("Wrong data")
        return render_template('forgottenPassword.html')

    new_password =  secrets. token_hex(10) 
    contents = [
        "Your password is changed to: ",
        new_password
    ]

    hashed_password = hashpw(new_password.encode('utf-8'), gensalt(4))
    try:
        db.hset(f"user:{login}", "password", hashed_password)
    except Exception:
        return make_response("something went wrong.", 500)   

    yag.send(email, 'Your new password', contents)
    return render_template('login.html')



@app.before_request
def get_logged_username():
    g.user = session.get('login')

def redirect(url, status=301):
    response = make_response("", 301)
    response.headers["Location"] = url
    return response

def is_user(login):
    return db.hexists(f"user:{login}", "password")

def is_the_same_email(login, email):
    email_from_db = db.hget(f"user:{login}", "email")
    return email == email_from_db.decode("utf-8")

def save_user(firstname, lastname, email, password, login):
    hashed_password = hashpw(password.encode('utf-8'), gensalt(4))
    db.hset(f"user:{login}", "firstname", firstname)
    db.hset(f"user:{login}", "lastname", lastname)
    db.hset(f"user:{login}", "email", email)
    db.hset(f"user:{login}", "password", hashed_password)

    db.hset('users', f'login_{login}',f'{login}')
    db.hset(f"user:{login}", "log", "T")
    return True



def verify_user(login, password):
    passwd = password.encode('utf-8')
    hashed = db.hget(f"user:{login}", "password")
    if not hashed:
        print(f"ERROR: no password for {login}")
        return False

    log = db.hget(f"user:{login}", "log").decode()
    if  checkpw(passwd, hashed):
        db.hset(f"user:{login}", "log",log + "T")
        return True
    else:
        db.hset(f"user:{login}", "log", log + "F")
        return False

    
def generate_token(user,user_role):
    payload = {
        "usr": user,
        "user_role": user_role,
        "aud":"aud",
        "exp": datetime.utcnow() + timedelta(seconds=JWT_EXP)
    }
    token = encode(payload, JWT_SECRET, algorithm='HS256')
    return token


def isStrong(password):
    n = 0
    if ((re.search('[a-z]', password) != None)):
        n += 26
    if ((re.search('[A-Z]', password)) != None):
        n += 26
    if ((re.search('[0-9]', password)) != None):
        n += 10
    if ((re.search('[^0-9a-zA-Z]+', password)) != None):
        n += 33

    e = len(password) * math.log2(n)

    if (e > 50): 
        return True

    else :
        return False

if __name__ == '__main__':
    app.run(threaded=True, port=5001, debug=True)


