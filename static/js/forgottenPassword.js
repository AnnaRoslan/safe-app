window.onload = () => {
    document.getElementById("email").addEventListener("focusout", () => validateEmail());
    document.getElementById("login").addEventListener("focusout", () => validateLogin());
    document.getElementById("buttonSubmit").onclick = () => Send();
}



function validateEmail() {
    var field = document.getElementById("email");
    if (field.value === "" || field.value.match(
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) === null) {
        field.classList.add("is-invalid");
        document.getElementById("emailText").textContent = "email is not valid";
        return false;
    } else {
        field.classList.remove("is-invalid");
        document.getElementById("emailText").textContent = "";
        return true;
    }
}


function validateLogin() {

    var field = document.getElementById("login");
    if (field.value === "") {
        field.classList.add("is-invalid");
        document.getElementById("loginText").textContent = "Please type login";
        return false
    }
    if (field.value.match(/^([a-zA-Z][0-9]*){4,12}$/) === null) {
        field.classList.add("is-invalid");
        document.getElementById("loginText").textContent = "Please type login";
        return false
    } else {
        field.classList.remove("is-invalid");
        document.getElementById("loginText").textContent = "";
        return true;
    }

}


function isValid() {
    return validateEmail() && validateLogin();
}


function Send() {
    return isValid()
}