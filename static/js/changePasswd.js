window.onload = () => {
    document.getElementById("oldPassword").addEventListener("focusout", () => validateOldPassword());
    document.getElementById("newPassword").addEventListener("focusout", () => validatePassword());
    document.getElementById("newPassword2").addEventListener("focusout", () => validatePassword2());
    document.getElementById("changePasswd").onclick = () => changePasswd();
}


function validateOldPassword() {
    var field = document.getElementById("oldPassword");
    if (field.value.match(/^(?=.*[a-z])?(?=.*[A-Z])?(?=.*\d)?(?=.*[@$!%*?&])?[A-Za-z\d@$!%*?&]{8,25}$/) === null) {
        field.classList.add("is-invalid");
        document.getElementById("oldPasswordText").textContent = "Please type password";
        return false
    } else {
        field.classList.remove("is-invalid");
        document.getElementById("oldPasswordText").textContent = "";
        return true;
    }
}

function validatePassword() {
    var field = document.getElementById("newPassword");
    validatePassword2()
    if (field.value.match(/^(?=.*[a-z])?(?=.*[A-Z])?(?=.*\d)?(?=.*[@$!%*?&])?[A-Za-z\d@$!%*?&]{8,25}$/) === null) {
        field.classList.add("is-invalid");
        document.getElementById("newPasswordText").textContent = "Password must be minimum 8 maximum 25 characters lenght. Password can contain letters numbers and special signs (@, $, !, %, *, #, ?, &) ";
        return false
    } else {
        field.classList.remove("is-invalid");
        document.getElementById("newPasswordText").textContent = "";
        return true;
    }
}

function validatePassword2() {
    var field = document.getElementById("newPassword2");
    var passwd = document.getElementById("newPassword");
    if (!(field.value == passwd.value) || field.value.match(/^(?=.*[a-z])?(?=.*[A-Z])?(?=.*\d)?(?=.*[@$!%*?&])?[A-Za-z\d@$!%*?&]{8,25}$/) === null) {
        field.classList.add("is-invalid");
        document.getElementById("newPassword2Text").textContent = "Password are not the same!";
        return false;
    } else {
        field.classList.remove("is-invalid");
        document.getElementById("newPassword2Text").textContent = "";
        return true;
    }
}


function isValid() {
    return validateOldPassword() && validatePassword() && validatePassword2()
}


function changePasswd() {
    isValid()
}