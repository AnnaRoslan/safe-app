window.onload = () => {
    document.getElementById("password").addEventListener("focusout", () => validatePassword());
    document.getElementById("login").addEventListener("focusout", () => validateLogin());
    document.getElementById("loginSubmit").onclick = () => login();
}



function validatePassword() {
    var field = document.getElementById("password");
    if (field.value.match(/^(?=.*[a-z])?(?=.*[A-Z])?(?=.*\d)?(?=.*[@$!%*?&])?[A-Za-z\d@$!%*?&]{8,25}$/) === null) {
        field.classList.add("is-invalid");
        document.getElementById("passwordText").textContent = "Please type password";
        return false;
    } else {
        field.classList.remove("is-invalid");
        document.getElementById("passwordText").textContent = "";
        return true;
    }
}

function validateLogin() {
    var field = document.getElementById("login");
    var isValid = false;

    if (field.value.match(/^([a-zA-Z][0-9]*){4,12}$/) === null) {
        field.classList.add("is-invalid");
        document.getElementById("loginText").textContent = "Login is not valid";
    } else {
        field.classList.remove("is-invalid");
        document.getElementById("loginText").textContent = "";
        isValid = true;
    }
    return isValid;
}

function isValid() {
    return validatePassword() && validateLogin();
}


function login() {
    return isValid();
}