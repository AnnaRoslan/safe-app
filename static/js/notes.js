window.onload = () => {
    if (document.getElementById('public-notes') != null) {
        public_messageEle = document.getElementById('public-notes');
        public_counterEle = document.getElementById('public-counter');

        public_messageEle.addEventListener('input', function(e) {
            const target = e.target;
            const maxLength = target.getAttribute('maxlength');
            const currentLength = target.value.length;

            public_counterEle.innerHTML = `${currentLength}/${maxLength}`;
        });

    }

    if (document.getElementById('notes') != null) {
        messageEle = document.getElementById('notes');
        counterEle = document.getElementById('counter');
        messageEle.addEventListener('input', function(e) {
            const target = e.target;
            const maxLength = target.getAttribute('maxlength');
            const currentLength = target.value.length;
            counterEle.innerHTML = `${currentLength}/${maxLength}`;
        });
    }

}


function create_public_notes() {
    var text = public_messageEle.value

    if (text == "")
        return false

    console.log("click")
    var url = window.location.href + '/public'
    var formData = new FormData();

    formData.append("note", text)
    var request = new XMLHttpRequest();
    request.open("POST", url);
    request.send(formData);
    public_messageEle.value = ""

}

function create_notes() {
    var text = messageEle.value
    user = document.getElementById('user').value
    if (text == "")
        return false

    console.log("click")
    var url = window.location.href
    var formData = new FormData();

    formData.append("note", text)
    formData.append("user", user)
    var request = new XMLHttpRequest();
    request.open("POST", url);
    request.send(formData);
    messageEle.value = ""

}

function delete_public_notes($this) {
    var id = $this.id
    var url = window.location.href + '/public'
    var formData = new FormData();
    formData.append("id", id)
    var request = new XMLHttpRequest();
    request.open("DELETE", url);
    request.send(formData);

}

function delete_notes($this) {
    var id = $this.id
    var url = window.location.href
    var formData = new FormData();

    formData.append("id", id)
    var request = new XMLHttpRequest();
    request.open("DELETE", url);
    request.send(formData);

}